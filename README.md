## Dependencies
### [alacritty](https://alacritty.org/)
```sh
brew install alacritty
```
Alternative [icon](https://www.dropbox.com/s/0i4ez0el7paksg3/Alacritty.icns?dl=0) for mac

### Nerd Fonts
Current: JetBrains Mono
```sh
brew tap homebrew/cask-fonts
brew install --cask font-jetbrains-mono-nerd-font
```

### [Zap](https://www.zapzsh.org/)
Simple zsh plugin manager by [chris@machine](https://github.com/ChristianChiarulli)
```sh
zsh <(curl -s https://raw.githubusercontent.com/zap-zsh/zap/master/install.zsh) --branch release-v1
```


### tmux & tpm
```sh
brew install tmux
```

then clone [tpm](https://github.com/tmux-plugins/tpm) into the directory containing `tmux.conf`. After the first terminal run press `prefix + I` to
install the plugins.

## Usage
_NOTE_: without explicit setting `stow` assumes that the parent directory is the target.
This works fine if machfiles are in `$HOME/` directory. In other cases an explicit target must be set:
```sh
stow --target=${HOME} /*
```

Import all:
```sh
stow /*
````

Import a specific directory:
```sh
stow zsh
```
